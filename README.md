# BARB-MODE

Barb-mode is a major mode for emacs to edit and interact with request
files for the [barb](https://gitlab.com/guillaume54/barb) HTTP query
tool.

![Animated demo](demo.gif)

## Installation

The supported method of installation is through the package manager
[straight.el](https://github.com/radian-software/straight.el). In
order to install barb-mode, add the following to your `init.el`:

```lisp
(straight-use-package
    '(barb-mode
      :type git
      :host gitlab 
      :repo "guillaume54/barb-mode"))
```

Then restart emacs.

## Usage

The mode will be automatically loaded whenever a `.barb` file is
visited. If you have chosen to use a different file extension, you can
start the mode by typing `M-x barb-mode`.

The mode provides syntax highlighting for the *barb* preamble and
allows easy execution of the request directly in emacs by typing `C-c
C-c`.

## Running a barb file

In order to execute a barb file, the
[barb](https://gitlab.com/guillaume54/barb#installation) executable
must be installed on your system and on the `PATH`. Then pressing `C-c
C-c` should work.

If your barb executable cannot be found or has a different name than
`barb`, you can configure *barb-mode* to use it instead like so:

```lisp
(setq barb-mode-exec "/path/to/my/barb")
```


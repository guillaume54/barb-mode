;;; barb-mode.el --- major mode for barb files -*- lexical-binding: t; -*-
(eval-when-compile
  (require 'rx))

(defconst barb--font-lock-defaults
  (let ((keywords '("GET" "POST" "PUT" "PATCH" "DELETE" "HEAD")))
    `(((,(rx-to-string `(: (or ,@keywords))) 0 font-lock-keyword-face)
       ("^#\\([[:word:]]+\\):" 1 font-lock-function-name-face)))))

(defvar barb-mode-syntax-table
  (let ((st (make-syntax-table)))
    st))

(defcustom barb-mode-exec "barb"
  "Path to or name of the `barb' executable.")

(defun barb-indent-line ()
  "Indent the current line - I do nothing."
  (save-excursion
    (back-to-indentation)
    (let ((start-point (point)))
      (unless (string= (string (char-after start-point)) "#")
        (move-beginning-of-line nil)
        (let* ((line-start (point))
               (indent (floor (/ (- start-point line-start) tab-width))))
          (indent-to (* (1+ indent) tab-width)))))
    (back-to-indentation)))

(defvar barb-mode-abbrev-table nil
  "Abbreviation table used by `barb-mode' buffers.")
(define-abbrev-table 'barb-mode-abbrev-table
  '())

(defun barb-run-file ()
  "Run the current file with `barb'."
  (interactive)
  (save-excursion
    (save-buffer)
    (let ((active-buf (current-buffer)))
      (with-output-to-temp-buffer "*barb*"
        (let ((buffer (get-buffer "*barb*")))
          (call-process barb-mode-exec nil buffer nil (buffer-file-name active-buf)))))))


(defvar barb-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "C-c C-c") #'barb-run-file)
    map))

;;;###autoload
(define-derived-mode barb-mode prog-mode "barb"
  "Major mode for barb files."
  :abbrev-table barb-mode-abbrev-table
  (setq font-lock-defaults barb--font-lock-defaults)
  ;(setq-local comment-start "##")
  (setq-local indent-line-function #'barb-indent-line)
  (setq-local indent-tabs-mode t))

;;;###autoload
(add-to-list 'auto-mode-alist '("\\.barb" . barb-mode))

(provide 'barb-mode)
